## Demo Project:
Automate Nexus Deployment
## Technologies used:
Ansible, Nexus, DigitalOcean, Java, Linux
## Project Decription:
* Create Server on DigitalOcean
* Write Ansible Playbook that creates Linux user for Nexus, configure server, installs and deploys Nexus and verifies that it is running successfully
## Description in details:
__Step 1:__ Create Server on DigitalOcean

### Write Ansible Playbook that creates Linux user for Nexus, configure server, installs and deploys Nexus and verifies that it is running successfully

__Before:__ Create new Playbook

__Step 1:__  Write 1st Play
```yaml 
- name: Install java and net-tools
  hosts: nexus_server
  tasks:
    - name: Update apt repo and cache
      ansible.builtin.apt: update_cache=yes force_apt_get=yes cache_valid_time=3600
    - name: Install Java 8
      ansible.builtin.apt: name=openjdk-8-jre-headless      
    - name: Install net-tools
      ansible.builtin.apt: name=net-tools 
```
__Step 2:__  Write 2nd Play
```yaml
- name: Download and unpack Nexus installer
  hosts: nexus_server
  tasks: 
    - name: Download Nexus
      get_url: 
        url: https://download.sonatype.com/nexus/3/latest-unix.tar.gz
        dest: /opt/
      register: download_result
    - name: untar nexus installer
      unarchive:
        src: "{{download_result.dest}}"
        dest: /opt/
        remote_src: yes
```
>__"get_url"__ module
>- Downloads files from HTTP, HTTPS or FTP to the remote server
>- Separate module for Windows targets: "win_get_url"

>- `{{download_result.dest}}` - get value from another module

__Step 3:__  "Find" Module 
```yaml 
- name: Download and unpack Nexus installer
  hosts: nexus_server
  tasks: 

  #banch of code

    - name: Find nexus folder
      ansible.builtin.find: 
        paths: /opt
        pattern: "nexus-*"
        file_type: directory
      register: find_result
    - name: Rename nexus folder
          ansible.builtin.shell: mv {{find_result.files[0].path}} /opt/nexus
```
>"Find" module
>- Return a list of based on specific criteria
>- For Windows targets: "win_find" module

>- `paths` - where you want to find
>- `pattern` - what you are looking for 

__Step 4:__  Conditionals 

If you run playbook above seconds time, you will get error that 'nexus' folder already exists. so you can create conditional:

```yaml
- name: Download and unpack Nexus installer
  hosts: nexus_server
  tasks:

  #banch of code

    - name: Find nexus folder
      ansible.builtin.find: 
        paths: /opt
        pattern: "nexus-*"
        file_type: directory
      register: find_result
    - name: Check if nexus flder already exists
      stat:
        path: /opt/nexus
      register: stat_result
    - debug: msg={{stat_result.stat.exists}}
    - name: Rename nexus folder
      ansible.builtin.shell: mv {{find_result.files[0].path}} /opt/nexus
      when: not stat_result.stat.exists
```
>__"stat" module__
>- Retrieve file or system status

>__Conditionals__
>- Execute task depending on some condition
>- Condition based on the value of a fact, a variable, or the result of a previous task

>__"stat" module__
>- Retrieve file or file system status

>__"when"__
>- Applies to a single task
>- No double curly braces needed here!
>- The `when` statement applies the test

__Step 5:__ Write 3rd Play

1. Create another Play
```yaml
- name: Create nexus user to own nexus folders
  hosts: NEXUS_SERVER 
  tasks: 
    - name: Ensure group nexus exists
      group:
        name: nexus
        state: present
    - name: Create nexus user
      user:
        name: nexus
        group: nexus
    - name: Make nexus user owner of nexus folder
      file:
        path: /opt/nexus
        state: directory
        owner: nexus
        group: nexus
        recurse: yes
    - name: Make nexus user owner of sonatype-work folder
      file:
        path: /opt/sonatype-work
        state: directory
        owner: nexus
        group: nexus
        recurse: yes
```
>__In Ansible__
>1. Create Nexus group
>2. Create Nexus user with group

>__"group" module__
>- Manage presence of groups on a host
>- For Windows targets:"win_group" module

>__"name" module__
>- Manage user accounts and user attributes
>- For Windows targets: "win_user" module

>__"file" module__
>- Managew files and file properties
>- For Windows targets: "win_file" module

__Step 4:__ Write 4rd party

1. Create new Play
```yaml
- name: Start nexus with nexus user
  hosts: NEXUS_SERVER
  become: True
  become_user: nexus
  tasks:
# Add line to file 
#    - name: Set run_as_user nexus
#      blockinfile:
#        path: /opt/nexus/bin/nexus.rc
#        block: |
#          run_as_user="nexus"

# Change lines
    - name: Set run_as_user nexus
      lineinline:
        path: /opt/nexus/bin/nexus.rc
          redexp: '^#run_as_user=""'
        line: run_as_user="nexus"
    - name: Start nexus
      command: /opt/nexus/bin/nexus start

```
>__"blockinfile" module__
>- Insert/update/remove a multiline text surrounded by customize marker lines 

>__"lineinline" module__
>- Ensures a particular line is in a file, or replace an existing line using regex
>- Useful when you want to change a __single line__ in a file only
>- See "replace" module to change multiple lines

>`regexp` syntax: '^TEXT_FROM_TARGET_LINE'

>__Note:__ If you dont specify condition it will execute this every run

2. Move `Check nexus folder stats` on top level
```yaml
- name: Download and unpack Nexus installer
  hosts: nexus_server
  tasks: 
    - name: Check if nexus flder already exists
      stat:
        path: /opt/nexus
      register: stat_result
```
3. Add `when`  `untar` block 
4. Change host name on every block on "group" that assigned in `hosts` file for efficient work (otherwise when you create new server you need change host name in every block manually)

__Step 5:__ Write 5th party
1. Create new block
```yaml
- name: Verify nexus running
  hosts: NEXUS_SERFER
  tasks:
    - name: Check with ps command
      shell: ps aux | grep nexus
      register: app_status
    - debug: msg={{app_status.stdout_lines}}
    - name: Wait one minute
      pause:
        minutes: 1
    - name: Check witch netstat
      shell: nenstat -plnt
      register: app_status
    - debug: msg={{app_status.stdout_lines}}
```