## Demo Project:
Structure Playbooks with Ansible Roles
## Technologies used:
Ansible, Docker, AWS, Linux
## Project Decription:
* Break up large Ansible Playbooks in to smaller manageable files using Ansible Roles
## Description in details:
__Step 1:__ Create Ansible Roles
1. Create new play (Copy `deploy-docker-new-user.yaml` and create file `deploy-docker-with-roles.yaml`)
1. Create folder for roles (you can find it in playbooks/roles)
2. Create folders for roles (inside folder `roles` create two folders: `create_user` and `start_containers`)
3. create folder `tasks` in those folders
4. inside `tasks` create file `main.yaml`
5. Now inside this file you can paste your tasks

_Example 1:_ Create user
```yaml
- name: Create new linux user
    ansible.builtin.user:
      name: YOUR_USER
      groups: adm,docker
```
_Example 2:_ Start containers
```yaml
- name: Copy docker compose
  ansible.builtin.copy:
    src: PATH_TO_YOUR_FILE/docker-compose-full.yaml
    dest: /home/YOUR_USER/docker-compose.yaml
- name: Docker login
  community.docker.docker_login: 
    registry_url: https://index.docker.io/v1/
    username: YOUR_USERNAME_HERE
    password: "{{docker_password}}"
- name: Start container from compose
  community.docker.docker_compose: 
    project_src: /home/YOUR_USER
```

__Step 2:__ Using Roles in Plays
1. Now in your playbook insted of `tasks` use `roles`
```yaml
- name: Create new linux user
  hosts: all
  become: yes
  roles:
    -  create_user
    
- name: Start docker containers
  hosts: all
  become: yes
  become_user: YOUR_USER
  vars_files:
    - project-vars
  roles:
    -  start_containers
```
__Note 1:__ Ansible find this roles  by itself because of the structure, because ansible knows to look for roles

__Note 2:__ you don't need to worry about variables because if you have reference on this (Example: you have variables in your play and you have reference on `projecr-vars` where values is)

__Example:__ Now you want to use your playbook with roles
1. Execute this command:
```sh
ansible-playbook deploy-docker-with-roles.yaml -i invetory_aws_ec2.yaml
```

__Step 3:__ Complete Roles

_Example:_ All static files that you coping around from one server to another, you want to hav them also in the role 
1. Create folder `files` in `start_containers` and create new file `docker-compose-full.yaml`(with necessary content) inside this folder
2. Now in your Play you can change referencing in `src` and just put here name of file
```yaml
src: docker-compose-full.yaml
```

__Step 4:__ Customize Roles with variables
1. Create folders `vars` inside role directories and create file that called `main.yaml` 
2. Inside this file you can define your variables

_Example:_ 
* `main.yaml` in tasks folder

```yaml
- name: Docker login
  community.docker.docker_login: 
    registry_url: "{{ docker_registry }}"
    username: "{{ docker_username }}"
    password: "{{ docker_password }}"
```
* `main.yaml` in vars folder
```yaml
docker_registry: your_docker_registry_or_default_registry_here
docker_username: your_user_here
```
__Note 1:__ `docker_password` you can define it in project-vars


__Note 2:__ You also can define default values
1. Create folders `default` inside role directories and create file that called `main.yaml`
2. Put there default values