## Demo Project:
Ansible & Docker
## Technologies used:
Ansible, AWS, Docker, Terraform, Linux
## Project Decription:
* Create AWS EC2 Instance with Terraform
* Write Ansible Playbook that installs necessary technologies like Docker and Docker Compose, copies docker-compose file to the server and starts the Docker containers configured inside the docker-composefile
## Description in details:

### Create AWS EC2 Instance with Terraform
__Step 1:__ Adjustment and creating
- Adjust variables for `main.tf`
- This configuration creating a new key pair from your local public key
- Remove `user_data`, anc create just 1 server
- terraaform init / terraform apply

### Write Ansible Playbook that installs necessary technologies like Docker and Docker Compose, copies docker-compose file to the server and starts the Docker containers configured inside the docker-composefile

__Step 1:__ Adjust inventory file
* Add new IP address  into `hosts` file

__Step 2:__ Write 1st Play (Install python3 and docker)
```yaml
---
- name: Install python3 docker an dcoker-compose
  hosts: docker_server
  become: yes
  become_user: root
  tasks:
    - name: Install python3 docker and docker compose
      vars:
        ansible_python_interpreter: /usr/bin/python    
      yum: 
        name: 
          - python3
          - docker
          - docker-compose
        update_cache: yes
        state: present

```
>- `update_cache` - equivalent apt update

>__Note:__ EC2 server use python v2 instead Python v3. Some features in Ansible requires Python v3
>- Configure `Ansible.cfg`
>```cfg
>[defaults]
>host_key_cheking = False
>
>interpreter_python = /usr/bin/pethon3
>```
>__Note 2:__ If you don't add `vars` in example above you will get error that system doesn't have python3 (on the first run), so you need to create variable that execute `install python3` using python v2
>
>__Note 3:__ `YUM` has the same problem. It uses python v2 by default

__IMPORTANT:__ `YUM` available to install Docker-compose now 

__Step 3:__ Write 2nd Play (Start docker daemon)

```yaml
- name: start docker
  hosts: docker_server
  become: yes
  become_user: root
  tasks:
    - name: start docker daemon
      systemd:
        name: docker
        state: started
```
>__systemd module__
>- controls systemd services on remote hosts

__Step 4__ Write 3rd Play (Add ec2-user to docker-group)
```yaml
- name: Add ec2-user to docker group
  hosts: docker_server
  become: yes
  become_user: root
  tasks:
    - name: Add ec2-user to docker group
      user:
        name: ec2-user
        groups: docker
        append: yes
    - name: Reconect to server
      meta: reset_connection


```
>`append` -  Adds docker group to list of ec2-user groups
>`meta` - module that fix problem with permisison (This problem happends because adding group takes effect when you recconect to the server after executing command)


__Step 5:__ Use "community.docker" Ansible Collection (Just showing example, delete it after test)

1. Add module
```yaml
- name: Install python3 docker an dcoker-compose
  hosts: docker_server
  become: yes
  become_user: root
  tasks:

  #banch of code

    - name: Install docker python module
      pip:
        name: 
         - docker
         - docker-compose

```

>__Docker community collection__
>- Includes many modules and plugins to work with Docker

__Step 6:__ Write 5th Play (Stars docker containers)
1. Write Play
```yaml
- name: start docker container
  hosts: docker_server
  vars_promt: 
    - name: docker_password
      prompt: Enter password for docker registry
  tasks:
    - name: Copy docker compose
      copy: 
        src: "absolute path to your dokcer-compose file here"
        dest: /home/ec2-user/docker-compose.yaml
    - name: docker login
      docker_login:
        registry_run: https://index.docker.io/v1/
        user_name: "user name for registry"
        password: "{{docker_password}}"
``` 
>__docker_login module__
>- similar to "docker login" command
>- authenticates woth a docker registry and adds the credentials to your local Docker config file

__Note:__ You can write password by yourself or use env vars file (Example: project-vars)
```yaml
- name: start docker container
  hosts: docker_server
  vars_file: 
    - name: project-vars
  tasks:
    - name: Copy docker compose
      copy: 
        src: "absolute path to your dokcer-compose file here"
        dest: /home/ec2-user/docker-compose.yaml
    - name: docker login
      docker_login:
        registry_run: https://index.docker.io/v1/
        user_name: "user name for registry"
        password: "{{docker_password}}"
``` 
3. docker-compose command
```yaml
- name: start docker container
  hosts: docker_server
  vars_file: 
    - name: project-vars
  tasks:

  #banch of code

    - name: Start container from commpose
      docker_compose: 
        project_src: /home/ec2_user
```
__Step 7:__ Execute Playbook on new server
```yaml
---
- name: Install python3 docker an dcoker-compose
  hosts: docker_server
  become: yes
  become_user: root
  gather_facts: false
  tasks:
    - name: Install python3 docker and docker compose
      vars:
        ansible_python_interpreter: /usr/bin/python    
      yum: 
        name: 
          - python3
          - docker
          - docker-compose
        update_cache: yes
        state: present
```
>-   `gather_facts: false` it tells ansible don't execute this task on the first run (other wise you will get error)
