## Demo Project:
Ansible Integration in Jenkins
## Technologies used:
Ansible, Jenkins, DigitalOcean, AWS, Boto3, Docker, Java, Maven, Linux, Git
## Project Decription:
* Create and configure a dedicated server for Jenkins
* Create and configure a dedicated server for Ansible Control Node
* Write Ansible Playbook, which configures 2 EC2 Instances
* Add ssh key file credentials in Jenkins for Ansible Control Node server and Ansible Managed Node servers
* Configure Jenkins to execute the Ansible Playbook on remote Ansible Control Node server as part of the CI/CD pipeline
* So the Jenkins file configuration will do the following:
  1. Connect to the remote Ansible Control Node server
  2. Copy Ansible playbook and configuration files to the remote Ansible Control Node server
  3. Copy the ssh keys for the Ansible Managed Node servers to the Ansible Control Node server
  4. Install Ansible, Python3 and Boto3 on the Ansible Control Node server
  5. With everything installed and copied to the remote Ansible Control Node server, execute the playbook remotely on that Control Node that will configure the 2 EC2 Managed Nodes
## Description in details:
### Create and configure a dedicated server for Ansible Control Node
__Step 1:__ Create droplet on DO for Ansible Control Node 
* Ubuntu
* 2 CPU, 60Gb

__Step 2:__ SSH on th server, install Ansible and nessesary packeges
1. apt update
2. apt install ansible
3. install python3
```sh
apt install python3-pip
```
4. install boto3 and boto core
```sh
pip install boto3 botocore
```
5. Configure AWS credentials
   * Create folder in home directory for credentials 
```sh
mkdir .aws
```
   * Create `credentials` file and paste here your credentials for AWS
### Write Ansible Playbook, which configures 2 EC2 Instances
__Step 1:__ Create 2 EC2 instances to be managed by Ansible
1. Open AWS UI and create 2 Amazon Linux servers (deffault settings, t2.micro)
2. Create new key-pair 

### Add ssh key file credentials in Jenkins for Ansible Control Node server and Ansible Managed Node servers
(In this Part use `java-maven-app` folder)

__Step 1:__ Copy Files from Jenkins to Ansible Server
1. Configure Jenkinsfile(create it)
   * clean it and create stage "copying files for Ansible"
2. Copy `inventory_aws_ec2.yaml` and `ansible.cfg`
3. Configure `ancible.cfg`
   * `inventory = inventory_aws_ec2.yaml` (because you dont have file 'hosts')
   * `privite_key_file = ~/ssh-key.pem` (AWS key. You will copy this file on machine from the pipelile)
4. Create Playbook in this folder
   * Write play for installing python3,docker and docker compose (Copy play from `deploy-docker-new-user.yaml`)
5. Configure Jenkinsfile(Credentials)
   * Here you will use ssh agent plugin(install it on your Jenkins server)
   * Give Jenkins privite key for sshing to Ansible server (add credentials in Jenkins)
   * Write logic to copy the files from Jenkins to ansible server
```groovy
script {
    echo "Copying all necessary files to ansible control node"
    sshagent(['name_of_created_credentials_for_privite_key']) {
        sh "scp -o StrictHostKeyCheking=no ansible/* root@ADRESS_OF_ANSIBLE_SERVER:/root/"
    }
}
```
6. Create new credentials for privite_key_file in Jenkins
>__Note:__ Jenkins not support new ssh format. For avoiding this problem you can conver your ssh key using this command
>```sh
>ssh-keygen -p -f /.ssh/file_that_you_want_to_convert -m pem -P "" -N ""
>```
7. Configure Jenkinsfile(add credentials for AWS)
```groovy
script {
     echo "Copying all necessary files to ansible control node "
     sshagent(['name_of_created_credentials_for_privite_key']) {
       sh "scp -o StrictHostKeyCheking=no ansible/* root@ADRESS_OF_ANSIBLE_SERVER:/root/"

        withCredentials([sshUserPrivateKey(credentialsId: 'name_of_aws_credentials', keyFileVariable: 'keyfile', usernameVariable: 'user')]) {
           sh 'scp $keyfile root@ADRESS_OF_ANSIBLE_SERVER:/root/ssh-key.pem'
        }
    }
}
```
### Configure Jenkins to execute the Ansible Playbook on remote Ansible Control Node server as part of the CI/CD pipeline
__Step 1:__ Create Jenkins Pipeline
1. Create pipeline in Jenkins UI
   * Everything is deffault
   * SCM: git
   * URL: your repository
   * credentials
   * branch name: 
   * Save and build

__Step 2:__ Execute Ansible Playbook from Jenkins
1. Create another stage in your `Jenkinsfile`
```groovy
strage("execute asnible playbook") {
    steps {
        script {
            echo "calling ansible playbook to configure EC2 instances"
            def remote: [:]
            remote.name = "ansible-server"
            remote.host = "ADRESS_OF_ANSIBLE_SERVER"
            remote.allowAnyHosts = true

            withCredentials([sshUserPrivateKey(credentialsId: 'name_of_ansible_key_credentials', keyFileVariable: 'keyfile', usernameVariable: 'user')]) {
                remote.user = user
                remote.identityFile = keyfile
                sshCommand remote: remote, command: "ansible-playbook my-playbook.yaml"    
            }
        }
    }
}
```
>- Here you need to install another plugin for executing command line commands on a remote server (Install this plugin: `ssh pipline step`)

__Step 3:__ Optimization (optional)
1. You can create variables for IP adresses 
```groovy
pipeline{
    agent any 
    environment {
        ANSIBLE_SERVER = "your_server_ip_here"
    }
    ...
    ...
}
```
2. Replace IP dress on variable
3. Automate the manual process  of configuring the ansible server
A. Write script (create sh file in project directory) 
B. Execute shell script 
```groovy
strage("execute asnible playbook") {
    steps {
        script {
            echo "calling ansible playbook to configure EC2 instances"
            def remote: [:]
            remote.name = "ansible-server"
            remote.host = "ADRESS_OF_ANSIBLE_SERVER"
            remote.allowAnyHosts = true

            withCredentials([sshUserPrivateKey(credentialsId: 'name_of_ansible_key_credentials', keyFileVariable: 'keyfile', usernameVariable: 'user')]) {
                remote.user = user
                remote.identityFile = keyfile
                sshScript remote: remote, script: "prepare-ansible-server.sh"
                sshCommand remote: remote, command: "ansible-playbook my-playbook.yaml"    
            }
        }
    }
}
```
Script:
```sh
#!/usr/bin/env bash

apt update
apt install ansible -y
apt install python3-pip -y
pip3 install boto3 botocore
```