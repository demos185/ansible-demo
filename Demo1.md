## Demo Project:
Automate Node.js application Deployment
## Technologies used:
Ansible, Node.js, DigitalOcean, Linux
## Project Decription:
* Create Server on DigitalOcean
* Write Ansible Playbook that installs necessary technologies, creates Linux userf or an application and deploys a NodeJS application with that user
## Description in details:
### Create Server on DigitalOcean

__Step 1:__ Create Droplet
1. Choose the smalless one
2. Ubuntu
3. Region

### Write Ansible Playbook that installs necessary technologies, creates Linux user or an application and deploys a NodeJS application with that user

__Step 1:__ Create Ansible project
1. Configure `hosts` file (URL, SSH key path, user)

__Step 2:__ Create new Playbook

1. Install npm and node
```yaml
- name: Install node and npm
  hosts: SERVER_URL
  tasks:
    - name: Update apt repo and cache
      ansible.builtin.apt: update_cache=yes force_apt_get=yes cache_valid_time=3600
    - name: Install nodejs and npm
      ansible.builtin.apt: 
        pkg:
          - nodejs
          - npm
```
 >- `cache_valid_time` -  Update the apt cache if its older than the `cache_valid_time`. This option is set in seconds.
 >- `pkg` - use this instead apt for installing more than 1 package 

2. Copy and unpack file
```yaml
- name: Deploy nodejs app
  hosts: SERVER_URL
  tasks:
    - name: Unpack nodejs file
      unarchive: 
        src: PATH_TO_THE_FILE
        dest: /root/
```
>- `src` file on your local machine
>- `dest` - here you assign path and how your file will be called
3. Install dependecies (executes inside `deploy nodejs app` block)
```yaml
  tasks:
    - name: Unpack nodejs file
      unarchive: 
        src: PATH_TO_THE_FILE
        dest: /root/
    - name: Install Dependecies
      npm:
        path: /root/package
    - name: Start the application
      command: 
       chdir: /root/package/app 
       cmd: node server 
      async: 1000
      poll: 0
```
>- `path` it will be execute on remote server (not on local machine)
>- If you want to run __multiple tasks__ in a playbook concurrently, use `async` and `poll`. When you set `poll: 0`, Ansible starts the task and immediately moves on to the next task without waiting for a result. Each async task runs until it either completes, fails or times out (runs longer than `async` value). The Playbook run ends without checking back on async tasks
4. Ensure App is running (executes inside `deploy nodejs app` block) 
```yaml
  tasks:
  
  #banch of code

    - name: Ensure app is running
      shell: ps aux | grep node
      register: app_status
    - debug: msg={{app_status.stdout_lines}}
```
>- `shell` - execute commands in the shell
>- `register` - creates variable and assigns that variable result of the module execution or the task execution

>__Return Values of Modules__
>- Ansible modules normally returns data
>- This data can be __changed into variable__

>__Debug module__
>- Print statements during execution
>- Useful for debugging variables or expressions

__Note:__ `command` and `shell` modules are not idempotent

__Step 3:__ Executing tasks with different user
1. Create new user

_Create this block under installing packages_
```yaml
- name: Create New Linux user for node app
  hosts: URL_HERE
  tasks:
    - name: Create Linux user
      user:
        name: YOUR_USER_NAME_HERE
        comment: your user admin
        group: admin
```
2. Chage destinations in another blocks
3. Execute commands as Linux user (change user)

```yaml
- name: Deploy nodejs app
  hosts: SERVER_URL
  become: True
  become_user: YOUR_USER_NAME_HERE 
  tasks:
    #banch of code
```
>__Privilege ecalation: become__
>- `become_user`  set to user with desired privileges. Default is _root_
>- `become` allows you to become another user, __different from the user that logged into the machine__
