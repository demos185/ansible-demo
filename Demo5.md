## Demo Project:
Configure Dynamic Inventory
## Technologies used:
Ansible, Terraform, AWS
## Project Decription:
* Create EC2 Instance with Terraform
* Write Ansible AWS EC2 Plugin to dynamically sets inventory of EC2 servers that Ansible should manage, instead of hard-coding server addresses in Ansible inventory file
## Description in details:
### Create EC2 Instance with Terraform
__Step 1:__ Use `main.tf` from demo3 and comfigure it:
1. delete user data
2. create 3 instances (2 instance 't2.small', one instance 't2.small')
3. terraform plan/terraform apply   
### Write Ansible AWS EC2 Plugin to dynamically sets inventory of EC2 servers that Ansible should manage, instead of hard-coding server addresses in Ansible inventory file
__Inventory Plugins vs. Inventory Script__
>__Plugins over scripts__
>- Inventory plugins make use of Ansible features
>- Inventory plugins written in __YAML__
>- Inventory scripts written in __Python__

>__Plugins/Scripts specific to infastracture provider__
>- You can use "ansible-doc -t inventory -l" to see list of available plugins
>- For AWS infrastructure, you need a specific plugin/script for AWS 
---
#### "aws_ec2" Plugin
__Step 1:__ Enable aws_ec2 plugin in `ansible.cfg`
```
enable_plugins = aws_ec2
```
__Step 2:__ Write plugin configuration
1. create new plugin configuration file `inventory_aws_ec2.yaml`
>__EC2 inventory source__
>- Config file must and with "aws_ec2.yaml"
```yaml
---
plugin: aws_ec2
regions:
  - eu-west-3
```
>__EC2 inventory source__
>- Get all hosts from specific region(s) 
2. Test inventory Plugin 
```sh
ansible-inventory -i inventory_aws_ec2.yaml --list
```
__Note:__ If you want to see just server hostname, you can change list on graph
```sh
ansible-inventory -i inventory_aws_ec2.yaml --graph
```
>__ansible-inventory__
>- Used to display or dump the configured inventory as Ansible sees it

__Step 3:__ Assign public DNS to EC2 Instances

>__Note:__ Without public DNS Ansible shows Privite DNS. In order to be able to connect to the servers using the privite DNS or privite IP adress, you ansible command would need to be excuted from inside the VPC where the servers are running

1. Open your `main.tf` and change configuration 
   * Destroy infrastructure
   * In VPC block add `enable_dns_hostnames = true`

Now Ansible will show you Public DNS names

__Step 4:__ Configure Ansible to use dynamic inventory
   1. In your playbook you need to change all `hosts` lines on existing group `aws_ec2`
```yaml
---
- name:  ...
  hosts: aws_ec2
  ...
  ...
  ...
```
   2. Add username and privite ssh key as parametres in ansible.cfg
   3. Execute command
```sh
ansible-playbook -i inventory_aws_ec2.yaml deploy-docker-new-user.yaml
```

__Note:__ Now if you add one more server (for example) you dont need chage anything in your playbook.  You just need run previous command again 

__Step 5:__ Target only specific servers
1. Destroy your current infrastructure and create 2 dev servers and 2 prod servers
2. Add `filters` into inventory file
```yaml
filters:
  tag:Name: dev*
```
__Note:__ Now this playbook will be executed only on `dev` servers

__Step 6:__ Create dynamic groups
1. Get rid of `filters` in inventory file
2. Add `keyd_groups` 
```yaml 
keyd_groups:
 - key: tags
```
3. now execute command and you will see groups
```sh
ansible-inventory -i inventory_aws_ec2.yaml --graph
```
4. For example, you want execute your playbook only on dev servers, you need change hosts in playbook on group name
```yaml
- name: ...
  hosts: group_name_here
  ...
  ...
```  