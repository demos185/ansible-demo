## Demo Project:
Ansible Integration in Terraform
## Technologies used:
Ansible, Terraform, AWS, Docker, Linux
## Project Decription:
* Create Ansible Playbook for Terraform integration
* Adjust Terraform configuration to execute Ansible Playbook automatically, so once Terraform provisions a server, it executes an Ansible playbook that configures the server
## Description in details:
### Create Ansible Playbook for Terraform integration

Use Playbook from previous demo

### Adjust Terraform configuration to execute Ansible Playbook automatically, so once Terraform provisions a server, it executes an Ansible playbook that configures the server

__Step 1:__ Open your `main.tf` file(use this file from previous demo) and configure it
1. Add privisioner(write it in the end of file)
```go
provisioner "local-exec"{
    working_dir = "YOUR_WORKING_DIR_HERE"
    command = "ansible-playbook --inventory ${self.public_ip}, --privite-key ${var.ssh_key_private} --user ec2-user YOUR_PLAY_BOOK"
}
```
2. change `hosts` in you playbook in each module (it need to be configured because terraform ignoring `hosts` file)
```yaml
hosts: all
```
>__Local-exec provisioner__
>- invokes a local executable after a resource is created

__Step 2:__ Avoiding itinilizing issue
1. add logic at the begining of your Playbook (check `wait_for` module [docs](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/wait_for_module.html))
```yaml
--- 
- name: Wiat for ssh connection
  hosts: all
  gather_facts: False
  tasks:
    - name: Ensure ssh port open
      wait_for:
        port: 22
        delay: 10
        timeout: 100
        search_regex: OpenSSH
        host: '{{ (ansible_ssh_host|default(ansible_host))|default(inventory_hostname) }}'
      vars: 
        ansible_connection: local 
        ansible_python_intepreter: /usr/bin/pethon
```   
>`host` - variable here puts values dinamically (this code taken from docs)

