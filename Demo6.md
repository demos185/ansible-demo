## Demo Project:
Automate Kubernetes Deployment
## Technologies used:
Ansible, Terraform, Kubernetes, AWS EKS, Python, Linux
## Project Decription:
* Create EKS cluster with Terraform
* Write Ansible Play to deploy application in a new K8s namespace
## Description in details:
### Create EKS cluster with Terraform
__Step 1:__ Create cluster with Terraform (use tf configuration from Terraform demo)
### Write Ansible Play to deploy application in a new K8s namespace
__Step 1:__ Create a Namespace in EKS Cluster
1. Open Ansible project and create new file `deploy-to-k8s.yaml`
2. Create Play
```yaml
---
- name: Deploy app in a new namespace
  hosts: localhost
  tasks:
    - name: Create a k8s namespace
      k8s:
        name: my-app
        api_verion: v1
        kind: namespace
        state: present
        kubeconfig: path_to_yourt_kubeconfig_file
```
>- `hosts: localhost` Use localcost because you execute ansible locally, but you are actually connecting to k8s cluster
>- `kubeconfig:` you assign your kubeconfig file here that tells which cluster to conncect to and how to connect to
3. Install Required packages
```sh
pip3 install openshift --user
pip3 install PyYAML --user
```
__Note:__ Openshift requires you to install that in your user not system-wide  
>__pip install__
>- pip defaults to installing python packeges to a system directory (such as /usr/loca/libpython3.7). - This __requires root access__
>- --user makes pip install packeges in your home directory instead - requires __no special privileges__

4. Before executing your Playbook, check `ancible.cfg` and change `inventory` value on `all` to avoid issues
5. export kubeconfig
6. kubectl get ns
   
__Step 2:__ Deploy app in new namespace
1. After creating k8s namespace, create another task
```yaml
- name: Deploy nginx app
  k8s:
    src: Path_to_your_nginx_file
    state: present
    kubeconfig: Path_to_your_kubeconfig
    namespace: my-app
```

__Step 3:__ Set environment variable for kubeconfig
1. You can export your kubeconfig via env variable
```sh
export K8S_AUTH_KUBECONFIG=Path_to_your_kubeconfig_here
```
2. Now you can get rid of kubeconfig line in you Plays
```yaml
---
- name: Deploy app in a new namespace
  hosts: localhost
  tasks:
    - name: Create a k8s namespace
      k8s:
        name: my-app
        api_verion: v1
        kind: namespace
        state: present
    - name: Deploy nginx app
      k8s:
        src: Path_to_your_nginx_file
        state: present
        namespace: my-app
```