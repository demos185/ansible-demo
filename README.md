## List of Demos:

__1.__ Automate Node.js application Deployment

__2.__ Automate Nexus Deployment

__3.__ Ansible & Docker

__4.__ Ansible Integration in Terraform

__5.__ Configure Dynamic Inventory

__6.__ Automate Kubernetes Deployment

__7.__ Ansible Integration in Jenkins

__8.__ Structure Playbooks with Ansible Roles

---

## Files and Folders:

__Playbooks__ - Playbooks for all Demos

__deploy_to_ec2__ - Demo3, Demo5

__java-maven-app__ - Demo7

__eks-tf__ - Demo6